package com.ioasys.empresasjava.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "USER")
public class User {
	
	@Id
	private String email;
	private String name;
	private Integer is_administrator;
	private Integer is_enabled;
	private String password;
	private String token;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getIs_administrator() {
		return is_administrator;
	}
	public void setIs_administrator(Integer is_administrator) {
		this.is_administrator = is_administrator;
	}
	public Integer getIs_enabled() {
		return is_enabled;
	}
	public void setIs_enabled(Integer is_enabled) {
		this.is_enabled = is_enabled;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}
