package com.ioasys.empresasjava.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MOVIE_HAS_DIRECTOR")
public class MovieHasDirector {

	@Id
	@EmbeddedId
	private MovieHasDirectorId movieHasDirectorId = new MovieHasDirectorId();

	public MovieHasDirectorId getMovieHasDirectorId() {
		return movieHasDirectorId;
	}

	public void setMovieHasDirectorId(MovieHasDirectorId movieHasDirectorId) {
		this.movieHasDirectorId = movieHasDirectorId;
	}

	
}
