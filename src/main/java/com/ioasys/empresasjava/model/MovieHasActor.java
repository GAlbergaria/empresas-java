package com.ioasys.empresasjava.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MOVIE_HAS_ACTOR")
public class MovieHasActor {

	@Id
	@EmbeddedId
	private MovieHasActorId movieHasActorId = new MovieHasActorId();

	public MovieHasActorId getMovieHasActorId() {
		return movieHasActorId;
	}

	public void setMovieHasActorId(MovieHasActorId movieHasActorId) {
		this.movieHasActorId = movieHasActorId;
	}

}
