package com.ioasys.empresasjava.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
@IdClass(MovieHasActorId.class)
public class MovieHasActorId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "id_actor_fk")
	private Actor Actor;

	@ManyToOne
	@JoinColumn(name = "id_movie_fk")
	private Movie movie;

	public Actor getActor() {
		return Actor;
	}

	public void setActor(Actor actor) {
		Actor = actor;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

}
