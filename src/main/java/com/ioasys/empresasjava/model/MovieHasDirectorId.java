package com.ioasys.empresasjava.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
@IdClass(MovieHasDirectorId.class)
public class MovieHasDirectorId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "id_director_fk")
	private Director director;

	@ManyToOne
	@JoinColumn(name = "id_movie_fk")
	private Movie movie;

	public Director getDirector() {
		return director;
	}

	public void setDirector(Director director) {
		this.director = director;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

}
