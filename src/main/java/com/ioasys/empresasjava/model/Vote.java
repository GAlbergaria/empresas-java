package com.ioasys.empresasjava.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VOTE")
public class Vote {

	@Id
	@EmbeddedId
	private VoteId vodeId = new VoteId();

	private int value;

	public VoteId getVodeId() {
		return vodeId;
	}

	public void setVodeId(VoteId vodeId) {
		this.vodeId = vodeId;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
