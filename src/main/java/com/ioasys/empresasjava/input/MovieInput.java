package com.ioasys.empresasjava.input;

import java.util.List;

public class MovieInput {

	private String title;
	private String description;
	private String id_administrator;
	private List<Integer> directors;
	private List<Integer> actors;
	private Integer genre;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getId_administrator() {
		return id_administrator;
	}

	public void setId_administrator(String id_administrator) {
		this.id_administrator = id_administrator;
	}

	public List<Integer> getDirectors() {
		return directors;
	}

	public void setDirectors(List<Integer> directors) {
		this.directors = directors;
	}

	public List<Integer> getActors() {
		return actors;
	}

	public void setActors(List<Integer> actors) {
		this.actors = actors;
	}

	public Integer getGenre() {
		return genre;
	}

	public void setGenre(Integer genre) {
		this.genre = genre;
	}

}
