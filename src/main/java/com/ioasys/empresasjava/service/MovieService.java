package com.ioasys.empresasjava.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.ioasys.empresasjava.input.MovieInput;
import com.ioasys.empresasjava.model.Actor;
import com.ioasys.empresasjava.model.Director;
import com.ioasys.empresasjava.model.Genre;
import com.ioasys.empresasjava.model.Movie;
import com.ioasys.empresasjava.model.MovieHasActor;
import com.ioasys.empresasjava.model.MovieHasActorId;
import com.ioasys.empresasjava.model.MovieHasDirector;
import com.ioasys.empresasjava.model.MovieHasDirectorId;
import com.ioasys.empresasjava.model.User;
import com.ioasys.empresasjava.output.MovieOutput;
import com.ioasys.empresasjava.output.MoviePageableOutput;
import com.ioasys.empresasjava.repository.ActorRepository;
import com.ioasys.empresasjava.repository.DirectorRepository;
import com.ioasys.empresasjava.repository.GenreRepository;
import com.ioasys.empresasjava.repository.MovieHasActorRepository;
import com.ioasys.empresasjava.repository.MovieHasDirectorRepository;
import com.ioasys.empresasjava.repository.MovieRepository;
import com.ioasys.empresasjava.repository.UserRepository;

@Service("MovieService")
public class MovieService {

	private final Integer IS_ADMINISTRATOR = 1;
	private final String RECOVERED_MOVIES = "Filmes recuperados com sucesso!";
	private final String MOVIE_ALREADY_EXIST = "Filme já cadastrado!";
	private final String MOVIE_REGISTERED = "Filme cadastrado com sucesso.";
	private final String USER_NOT_AUTHORIZED = "Usuário não autorizado para cadastrar filmes!";


	@Autowired
	private MovieRepository movieRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private GenreRepository genreRepository;
	@Autowired
	private DirectorRepository directorRepository;
	@Autowired
	private MovieHasDirectorRepository movieHasDirectorRepository;
	@Autowired
	private ActorRepository actorRepository;
	@Autowired
	private MovieHasActorRepository movieHasActorRepository;

	public MoviePageableOutput getAllMovies() {
		MoviePageableOutput moviePageableOutput = new MoviePageableOutput();
		int page = 0;
		int size = 10;

		PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "title");

		Page<Movie> pages = movieRepository.getAllMovies(pageRequest);

		moviePageableOutput.setMovies(pages);
		moviePageableOutput.addMessageResult(RECOVERED_MOVIES);
		return moviePageableOutput;

	}

	public MovieOutput createMovie(MovieInput movieInput) {
		MovieOutput movieOutput = new MovieOutput();
		
		Optional<User> user = userRepository.findById(movieInput.getId_administrator());
		if(user.isPresent() && !user.get().getIs_administrator().equals(IS_ADMINISTRATOR)){
			movieOutput.addResult(USER_NOT_AUTHORIZED);
			return movieOutput;
		}
		Optional<Movie> movie = movieRepository.getMovieByTitle(movieInput.getTitle());
		if (movie.isPresent()) {
			movieOutput.addResult(MOVIE_ALREADY_EXIST);
			return movieOutput;
		}
		Optional<Genre> genre = genreRepository.findById(movieInput.getGenre());

		Movie newMovie = new Movie();
		newMovie.setTitle(movieInput.getTitle());
		newMovie.setDescription(movieInput.getDescription());
		newMovie.setGenre(genre.get());
		movieRepository.save(newMovie);

		for (Integer director : movieInput.getDirectors()) {
			Optional<Director> repositoryDirector = directorRepository.findById(director);

			MovieHasDirectorId movieHasDirectoryId = new MovieHasDirectorId();
			movieHasDirectoryId.setDirector(repositoryDirector.get());
			movieHasDirectoryId.setMovie(newMovie);

			MovieHasDirector movieHasDirectory = new MovieHasDirector();
			movieHasDirectory.setMovieHasDirectorId(movieHasDirectoryId);

			movieHasDirectorRepository.save(movieHasDirectory);
		}

		for (Integer actor : movieInput.getActors()) {
			Optional<Actor> repositoryActor = actorRepository.findById(actor);

			MovieHasActorId movieHasActorId = new MovieHasActorId();
			movieHasActorId.setActor(repositoryActor.get());
			movieHasActorId.setMovie(newMovie);

			MovieHasActor movieHasActor = new MovieHasActor();
			movieHasActor.setMovieHasActorId(movieHasActorId);
			movieHasActorRepository.save(movieHasActor);
		}
		movieOutput.addResult(MOVIE_REGISTERED);
		movieOutput.setMovie(newMovie);

		return movieOutput;
	}
}
