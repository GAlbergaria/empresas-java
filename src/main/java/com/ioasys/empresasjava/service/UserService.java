package com.ioasys.empresasjava.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.ioasys.empresasjava.input.UserInput;
import com.ioasys.empresasjava.model.User;
import com.ioasys.empresasjava.output.UserOutput;
import com.ioasys.empresasjava.output.UserOutputBody;
import com.ioasys.empresasjava.output.UserPageableOutput;
import com.ioasys.empresasjava.repository.UserRepository;

@Service("UserService")
public class UserService {

	private final String USER_DELETED = "Usuário deletado com sucesso!";
	private final String USER_DISABLED = "Usuário Desativado!";
	private final String NOT_FOUND_USER = "Usuário não encontrado!";
	private final String EMAIL_ALREADY_REGISTERED = "Email informado já foi cadastro!";
	private final Integer NOT_ACTIVE_USER = 0;
	private final Integer ACTIVE_USER = 1;
	private final String SUCCESSFULLY_REGISTERED_USER = "Usuário registrado com Sucesso!";
	private final String SUCCESSFULLY_UPDATE_USER = "Usuário atualizado com Sucesso!";
	private final String RECOVERED_USERS = "Usuários recuperados com sucesso!";

	
	@Autowired
	private UserRepository userRepository;

	public UserOutput createUser(UserInput userInput, int administrator) {
		UserOutput userOutput = new UserOutput();
		Optional<User> user = userRepository.findById(userInput.getEmail());

		if (user.isPresent()) {
			userOutput.addMessageResult(EMAIL_ALREADY_REGISTERED);
			if (user.get().getIs_enabled().equals(NOT_ACTIVE_USER)) {
				userOutput.addMessageResult(USER_DISABLED);
			}
		} else {
			User newUser = new User();
			BeanUtils.copyProperties(userInput, newUser);
			newUser.setIs_administrator(administrator);
			newUser.setIs_enabled(ACTIVE_USER);
			userRepository.save(newUser);
			UserOutputBody usuarioOutputBody = new UserOutputBody();
			BeanUtils.copyProperties(newUser, usuarioOutputBody);
			usuarioOutputBody.setIs_enabled(true);
			userOutput.setUser(usuarioOutputBody);
			userOutput.addMessageResult(SUCCESSFULLY_REGISTERED_USER);
		}
		return userOutput;
	}

	public UserPageableOutput getAllUsers() {
		UserPageableOutput userPageableOutput = new UserPageableOutput();
		int page = 0;
		int size = 10;

		PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "name");
		Page<User> userPage = userRepository.getAllUsers(pageRequest);
		if (!userPage.isEmpty()) {
			userPageableOutput.addMessageResult(RECOVERED_USERS);
			userPageableOutput.setUsers(userPage);
		}
		return userPageableOutput;
	}

	public UserOutput updateUser(UserInput userInput, String email) {
		UserOutput userOutput = new UserOutput();
		Optional<User> oldUser = userRepository.findById(email);

		if (oldUser.isPresent() && oldUser.get().getIs_enabled().equals(1)) {
			User user = new User();
			user = oldUser.get();
			Optional<User> updateUser = userRepository.findById(userInput.getEmail());
			if (updateUser.isPresent()) {
				userOutput.addMessageResult(EMAIL_ALREADY_REGISTERED);
			} else {
				user.setEmail(userInput.getEmail());
			}
			user.setName(userInput.getName());
			user.setPassword(userInput.getPassword());
			user.setToken(null);
			userRepository.save(user);
			UserOutputBody usuarioOutputBody = new UserOutputBody();
			BeanUtils.copyProperties(user, usuarioOutputBody);
			usuarioOutputBody.setIs_enabled(true);
			userOutput.setUser(usuarioOutputBody);
			userOutput.addMessageResult(SUCCESSFULLY_UPDATE_USER);
		} else if (oldUser.isPresent() && oldUser.get().getIs_enabled().equals(NOT_ACTIVE_USER)) {
			userOutput.addMessageResult(USER_DISABLED);
		} else {
			userOutput.addMessageResult(NOT_FOUND_USER);
		}
		return userOutput;
	}

	public UserOutput logicalExclusion(String email) {
		UserOutput userOutput = new UserOutput();

		Optional<User> user = userRepository.findById(email);

		if (user.isPresent()) {
			User userDeleted = user.get();
			userDeleted.setIs_enabled(NOT_ACTIVE_USER);
			userRepository.save(userDeleted);
			userOutput.addMessageResult(USER_DELETED);
		}
		return userOutput;
	}

}
