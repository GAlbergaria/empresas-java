package com.ioasys.empresasjava.output;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class UserOutput {

	private List<String> results = new ArrayList<String>();
	private UserOutputBody user;
	private List<UserOutputBody> users;

	public List<String> getResults() {
		return results;
	}

	public void setResults(List<String> results) {
		this.results = results;
	}
	
	public void addMessageResult(String message) {
		this.results.add(message);
	}
	
	public UserOutputBody getUser() {
		return user;
	}

	public void setUser(UserOutputBody user) {
		this.user = user;
	}

	public List<UserOutputBody> getUsers() {
		return users;
	}

	public void setUsers(List<UserOutputBody> users) {
		this.users = users;
	}

}
