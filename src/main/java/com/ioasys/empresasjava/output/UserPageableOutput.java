package com.ioasys.empresasjava.output;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ioasys.empresasjava.model.User;

@JsonInclude(Include.NON_NULL)
public class UserPageableOutput {

	private Page<User> users;
	private List<String> result = new ArrayList<String>();

	public Page<User> getUsers() {
		return users;
	}

	public void setUsers(Page<User> users) {
		this.users = users;
	}

	public List<String> getResult() {
		return result;
	}

	public void setResult(List<String> result) {
		this.result = result;
	}

	public void addMessageResult(String message) {
		this.result.add(message);
	}

}
