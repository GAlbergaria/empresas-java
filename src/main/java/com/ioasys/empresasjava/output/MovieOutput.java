package com.ioasys.empresasjava.output;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ioasys.empresasjava.model.Movie;

@JsonInclude(Include.NON_NULL)
public class MovieOutput {

	private List<String> results = new ArrayList<String>();
	private Movie movie;
	private List<Movie> movies;

	public List<String> getResults() {
		return results;
	}

	public void setResults(List<String> results) {
		this.results = results;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public List<Movie> getMovies() {
		return movies;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}

	public void addResult(String message) {
		this.results.add(message);
	}

}
