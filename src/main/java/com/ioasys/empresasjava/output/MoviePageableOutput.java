package com.ioasys.empresasjava.output;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.ioasys.empresasjava.model.Movie;

@JsonInclude(Include.NON_NULL)
public class MoviePageableOutput {

	private Page<Movie> movies;
	private List<String> result = new ArrayList<String>();

	public Page<Movie> getMovies() {
		return movies;
	}

	public void setMovies(Page<Movie> movies) {
		this.movies = movies;
	}

	public List<String> getResult() {
		return result;
	}

	public void setResult(List<String> result) {
		this.result = result;
	}

	public void addMessageResult(String message) {
		this.result.add(message);
	}

}
