package com.ioasys.empresasjava.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ioasys.empresasjava.model.Movie;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Integer>{

	@Query(value = "SELECT m.* FROM MOVIE m order by m.title ASC", nativeQuery = true)
	Page<Movie> getAllMovies(Pageable pageable);
	
	@Query(value = "SELECT m.* FROM MOVIE m WHERE m.title = ?1", nativeQuery = true)
    Optional<Movie> getMovieByTitle(String title);
	
}
