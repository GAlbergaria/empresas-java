package com.ioasys.empresasjava.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ioasys.empresasjava.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

	@Query(value = "SELECT u.* FROM USER u " + "WHERE u.is_administrator <> 1 order by u.name ASC", nativeQuery = true)
	Page<User> getAllUsers(Pageable pageable);
}
