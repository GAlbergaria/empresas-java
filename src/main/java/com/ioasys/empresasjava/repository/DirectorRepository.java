package com.ioasys.empresasjava.repository;

import org.springframework.data.repository.CrudRepository;

import com.ioasys.empresasjava.model.Director;

public interface DirectorRepository extends CrudRepository<Director, Integer>{

}
