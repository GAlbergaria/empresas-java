package com.ioasys.empresasjava.repository;

import org.springframework.data.repository.CrudRepository;

import com.ioasys.empresasjava.model.Genre;

public interface GenreRepository extends CrudRepository<Genre, Integer>{

}
