package com.ioasys.empresasjava.repository;

import org.springframework.data.repository.CrudRepository;

import com.ioasys.empresasjava.model.MovieHasActor;
import com.ioasys.empresasjava.model.MovieHasActorId;

public interface MovieHasActorRepository extends CrudRepository<MovieHasActor, MovieHasActorId>{

}
