package com.ioasys.empresasjava.repository;

import org.springframework.data.repository.CrudRepository;

import com.ioasys.empresasjava.model.MovieHasDirector;
import com.ioasys.empresasjava.model.MovieHasDirectorId;

public interface MovieHasDirectorRepository extends CrudRepository<MovieHasDirector, MovieHasDirectorId>{

}
