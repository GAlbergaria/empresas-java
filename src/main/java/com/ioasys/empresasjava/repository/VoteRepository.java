package com.ioasys.empresasjava.repository;

import org.springframework.data.repository.CrudRepository;

import com.ioasys.empresasjava.model.Vote;
import com.ioasys.empresasjava.model.VoteId;

public interface VoteRepository extends CrudRepository<Vote, VoteId>{

}
