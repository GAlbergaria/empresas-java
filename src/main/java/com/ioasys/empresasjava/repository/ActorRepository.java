package com.ioasys.empresasjava.repository;

import org.springframework.data.repository.CrudRepository;

import com.ioasys.empresasjava.model.Actor;

public interface ActorRepository extends CrudRepository<Actor, Integer>{

}
