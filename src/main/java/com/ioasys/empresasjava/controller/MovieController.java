package com.ioasys.empresasjava.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ioasys.empresasjava.input.MovieInput;
import com.ioasys.empresasjava.output.MovieOutput;
import com.ioasys.empresasjava.output.MoviePageableOutput;
import com.ioasys.empresasjava.service.MovieService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Movie Controller")
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api")
public class MovieController {

	@Autowired
	private MovieService movieService;

	@ApiOperation(value = "Criar filme")
	@PostMapping("/movie")
	public ResponseEntity<MovieOutput> createMovie(@RequestBody MovieInput movieInput) {
		MovieOutput movieOutput = new MovieOutput();
		movieOutput = movieService.createMovie(movieInput);
		if (movieOutput.getMovie() != null)
			return new ResponseEntity<MovieOutput>(movieOutput, HttpStatus.OK);
		else
			return new ResponseEntity<MovieOutput>(movieOutput, HttpStatus.UNAUTHORIZED);
	}

	@GetMapping("/movies")
	@ApiOperation(value = "Recupera todos os filmes cadastrados")
	public ResponseEntity<MoviePageableOutput> GetAllUsers() {
		MoviePageableOutput userPageableOutput = new MoviePageableOutput();
		userPageableOutput = movieService.getAllMovies();

		if (userPageableOutput.getMovies() != null)
			return new ResponseEntity<MoviePageableOutput>(userPageableOutput, HttpStatus.OK);
		else
			return new ResponseEntity<MoviePageableOutput>(userPageableOutput, HttpStatus.NOT_FOUND);
	}

}
