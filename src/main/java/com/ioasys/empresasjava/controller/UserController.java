package com.ioasys.empresasjava.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ioasys.empresasjava.input.UserInput;
import com.ioasys.empresasjava.output.UserOutput;
import com.ioasys.empresasjava.output.UserPageableOutput;
import com.ioasys.empresasjava.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "User Controller")
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api")
public class UserController {

	private final Integer IS_ADMINISTRATOR = 0;

	@Autowired
	private UserService userService;

	@ApiOperation(value = "Cria usuário")
	@PostMapping("/user")
	public ResponseEntity<UserOutput> createUser(@RequestBody UserInput userInput) {
		UserOutput userOutput = new UserOutput();
		userOutput = userService.createUser(userInput, IS_ADMINISTRATOR);
		if (userOutput.getUser() != null)
			return new ResponseEntity<UserOutput>(userOutput, HttpStatus.OK);
		else
			return new ResponseEntity<UserOutput>(userOutput, HttpStatus.UNAUTHORIZED);
	}

	@GetMapping("admin/users")
	@ApiOperation(value = "Recupera todos os usuários cadastrados")
	public ResponseEntity<UserPageableOutput> GetAllUsers() {
		UserPageableOutput userPageableOutput = new UserPageableOutput();
		userPageableOutput = userService.getAllUsers();

		if (userPageableOutput.getUsers() != null)
			return new ResponseEntity<UserPageableOutput>(userPageableOutput, HttpStatus.OK);
		else
			return new ResponseEntity<UserPageableOutput>(userPageableOutput, HttpStatus.NOT_FOUND);
	}

	@ApiOperation(value = "Atualiza o usuário.")
	@PutMapping("user/update/{email}")
	public ResponseEntity<UserOutput> updateUser(@PathVariable(value = "email") String email,
			@RequestBody UserInput usuarioInput) {
		UserOutput usuarioOutput = new UserOutput();
		usuarioOutput = userService.updateUser(usuarioInput, email);
		if (usuarioOutput.getUser() != null)
			return new ResponseEntity<UserOutput>(usuarioOutput, HttpStatus.OK);
		else
			return new ResponseEntity<UserOutput>(usuarioOutput, HttpStatus.NOT_FOUND);
	}

	@ApiOperation(value = "Deleta usuário")
	@DeleteMapping("/user/{email}")
	public ResponseEntity<UserOutput> deleteUser(@PathVariable(value = "email") String email) {

		UserOutput userOutput = new UserOutput();
		userOutput = userService.logicalExclusion(email);
		if (userOutput.getResults() != null)
			return new ResponseEntity<UserOutput>(userOutput, HttpStatus.OK);
		else
			return new ResponseEntity<UserOutput>(userOutput, HttpStatus.NOT_FOUND);

	}
}
