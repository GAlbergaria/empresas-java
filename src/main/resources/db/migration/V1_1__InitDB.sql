CREATE TABLE IF NOT EXISTS `ACTOR`(
	id_actor int auto_increment,
	name varchar(20) not null,
	dt_nasc DATE not null,
	constraint Actor_pk
		primary key (id_actor)
)ENGINE=InnoDB DEFAULT CHARSET=UTF8;


CREATE TABLE IF NOT EXISTS `GENRE`(

	id_genre int auto_increment,
	name varchar(20) not null,
	constraint Genre_pk
		primary key (id_genre)
)ENGINE=InnoDB DEFAULT CHARSET=UTF8;


    
CREATE TABLE IF NOT EXISTS `DIRECTOR`(
	id_director int not null auto_increment,
	name varchar(20) null,
	birth_date DATE not null,
	constraint Director_pk
		primary key (id_director)
)ENGINE=InnoDB DEFAULT CHARSET=UTF8;


CREATE TABLE IF NOT EXISTS `MOVIE`(
	id_movie int not null auto_increment,
	title varchar(30) not null,
	description varchar(255) not null,
	id_genre_fk int not null,
	constraint Movie_pk
		primary key (id_movie),
	constraint Movie_Genre_fk
		foreign key (id_genre_fk) references Genre (id_genre)
			on update cascade on delete cascade
)ENGINE=InnoDB DEFAULT CHARSET=UTF8;


CREATE TABLE IF NOT EXISTS `USER`(
	email varchar(255) not null,
	is_administrator int not null,
	is_enabled int not null,
	password varchar(255) not null,
	token varchar(255),
	name varchar(60) not null,
	constraint USER_pk
		primary key (email)

)ENGINE=InnoDB DEFAULT CHARSET=UTF8;

CREATE TABLE IF NOT EXISTS `VOTE` 
(
	id_movie_fk int not null,
	id_user_fk VARCHAR(255) not null,
	value int not null,
	constraint VOTE_pk
		primary key (id_movie_fk,id_user_fk),
	constraint VOTE_MOVIE__fk
		foreign key (id_movie_fk) references MOVIE (id_movie)
			on update cascade on delete cascade,
	constraint VOTE_USER__fk
		foreign key (id_user_fk) references USER (email)
)ENGINE=InnoDB DEFAULT CHARSET=UTF8;


CREATE TABLE IF NOT EXISTS `MOVIE_HAS_ACTOR` 
(
	id_movie_fk int not null,
	id_actor_fk int not null,
	constraint MOVIE_HAS_ACTOR_pk
		primary key (id_movie_fk,id_actor_fk),
	constraint MOVIE_HAS_ACTOR_ACTOR__fk
		foreign key (id_actor_fk) references ACTOR (id_actor)
			on update cascade on delete cascade,
	constraint MOVIE_HAS_ACTOR_MOVIE__fk
		foreign key (id_movie_fk) references MOVIE (id_movie)
			on update cascade on delete cascade
)ENGINE=InnoDB DEFAULT CHARSET=UTF8;


CREATE TABLE IF NOT EXISTS `MOVIE_HAS_DIRECTOR`
(
	id_movie_fk int not null,
	id_director_fk int not null,
	constraint MOVIE_HAS_DIRECTOR_pk
		primary key (id_movie_fk,id_director_fk),
	constraint MOVIE_HAS_DIRECTOR_DIRECTOR__fk
		foreign key (id_director_fk) references DIRECTOR (id_director)
			on update cascade on delete cascade,
	constraint MOVIE_HAS_DIRECTOR_MOVIE__fk
		foreign key (id_movie_fk) references MOVIE (id_movie)
			on update cascade on delete cascade
)ENGINE=InnoDB DEFAULT CHARSET=UTF8;

INSERT INTO db_mysql.GENRE (id_genre, name) VALUES (1, 'Romance');
INSERT INTO db_mysql.GENRE (id_genre, name) VALUES (2, 'Ação');
INSERT INTO db_mysql.GENRE (id_genre, name) VALUES (3, 'Aventura');
INSERT INTO db_mysql.GENRE (id_genre, name) VALUES (4, 'Comédia');

INSERT INTO db_mysql.DIRECTOR (id_director, name, birth_date) VALUES (1, 'Martin Scorsess', '1942-11-17');
INSERT INTO db_mysql.DIRECTOR (id_director, name, birth_date) VALUES (2, 'Steven Spielberg', '1946-12-18');
INSERT INTO db_mysql.DIRECTOR (id_director, name, birth_date) VALUES (3, 'Steven Spielberg', '1963-03-27');
INSERT INTO db_mysql.DIRECTOR (id_director, name, birth_date) VALUES (4, 'Francis Ford Coppol', '1939-04-07');

INSERT INTO db_mysql.ACTOR (id_actor, name, dt_nasc) VALUES (1, 'Will Smith', '1968-09-25');
INSERT INTO db_mysql.ACTOR (id_actor, name, dt_nasc) VALUES (2, 'Brad Pitt', '1963-12-18');
INSERT INTO db_mysql.ACTOR (id_actor, name, dt_nasc) VALUES (3, 'Keanu Reeves', '1964-09-02');
INSERT INTO db_mysql.ACTOR (id_actor, name, dt_nasc) VALUES (4, 'Angelina Jolie', '195-06-04');
INSERT INTO db_mysql.ACTOR (id_actor, name, dt_nasc) VALUES (5, 'Juliana Paes', '1969-03-26');

INSERT INTO db_mysql.MOVIE (id_movie, title, description, id_genre_fk) VALUES (1, 'Extraordinário', 'Auggie Pullman é um garoto que nasceu com uma deformidade facial e precisou passar por 27 cirurgias plásticas. Aos 10 anos, ele finalmente começa a frequentar uma escola regular, como qualquer outra criança, pela primeira vez. No quinto ano', 1);
INSERT INTO db_mysql.MOVIE (id_movie, title, description, id_genre_fk) VALUES (2, 'Maze Runner: A Cura Mortal', 'Thomas embarca em uma missão para encontrar a cura para uma doença mortal e descobre que os', 2);

INSERT INTO db_mysql.USER (email, is_administrator, is_enabled, password, token, name) VALUES ('gabrielle.rosane.santos@gmail.com', 0, 1, '123456789', null, 'Gabrielle Rosane Santos Albergaria');
INSERT INTO db_mysql.USER (email, is_administrator, is_enabled, password, token, name) VALUES ('71800921@aluno.faculdadecotemig.br', 1, 1, '987654321', null, 'Administrador');
INSERT INTO db_mysql.USER (email, is_administrator, is_enabled, password, token, name) VALUES ('lucas.henrique@gmail.com', 0, 1, '123456789', null, 'Lucas Henrique');

INSERT INTO db_mysql.MOVIE_HAS_DIRECTOR (id_movie_fk, id_director_fk) VALUES (2, 2);
INSERT INTO db_mysql.MOVIE_HAS_DIRECTOR (id_movie_fk, id_director_fk) VALUES (1, 2);
INSERT INTO db_mysql.MOVIE_HAS_DIRECTOR (id_movie_fk, id_director_fk) VALUES (1, 1);

INSERT INTO db_mysql.MOVIE_HAS_ACTOR (id_movie_fk, id_actor_fk) VALUES (1, 1);
INSERT INTO db_mysql.MOVIE_HAS_ACTOR (id_movie_fk, id_actor_fk) VALUES (1, 2);
INSERT INTO db_mysql.MOVIE_HAS_ACTOR (id_movie_fk, id_actor_fk) VALUES (2, 2);
INSERT INTO db_mysql.MOVIE_HAS_ACTOR (id_movie_fk, id_actor_fk) VALUES (1, 3);
INSERT INTO db_mysql.MOVIE_HAS_ACTOR (id_movie_fk, id_actor_fk) VALUES (2, 3);

INSERT INTO db_mysql.VOTE (id_movie_fk, id_user_fk, value) VALUES (1, 'gabrielle.rosane.santos@gmail.com', 2);
INSERT INTO db_mysql.VOTE (id_movie_fk, id_user_fk, value) VALUES (2, 'gabrielle.rosane.santos@gmail.com', 4);